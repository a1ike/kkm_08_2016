$(document).ready(function () {

    $('.carousel').carousel({
        interval: false
    });

    $('#owl-products').owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds

        items: 6,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]

    });

    $('#owl-clients').owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds

        items: 6,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]

    });

});